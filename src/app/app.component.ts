import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as moment from 'moment';
import {MySqlService} from './my-sql.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AppComponent implements OnInit {
  title = 'Система водопостачання';

  devices: any = [];
  pipe_repair_dates: any[] = [];

  table_values: any[] = [];
  table_water: any[] = [];
  table_dates: any[] = [];

  multi: any[] = [];
  multi2: any[] = [];

  showMessages: boolean[] = [false, false, false];
  messages: string[] = [];

  devicePositionsArray: any[] = [];

  selectedDevice = 0;

  constructor(private mySql: MySqlService) {
  }

  async ngOnInit(): Promise<void> {
    let deviceId: number = 0;
    let pipeId: number = 0;
    let history: any = [];
    let dates: string[] = [];
    let dates2: string[] = [];
    let values: number[] = [];
    let values2: number[] = [];
    let water: number[] = [];
    let water2: number[] = [];

    this.devices = await this.mySql.getAllDevices().toPromise();

    this.devicePositionsArray = this.devices.map((device: any) => {
      return {x: device.coordinate_x, y: device.coordinate_y}
    })
    

    for (let i = 0; i < this.devices.length; i++) {
      deviceId = this.devices[i].id_device;

      history = await this.mySql.getHistory(deviceId).toPromise();

      dates = history.map((el: { date: Date; }) => moment(el.date).format('YYYY-MM-DD HH:mm'));
      this.table_dates[i] = dates;
      dates2 = getDates(dates);

      values = history.map((el: { reading_value: any; }) => el.reading_value);
      this.table_values[i] = values;
      values2 = getForecast(values);

      water = history.map((el: { water_consumption: any; }) => el.water_consumption);
      this.table_water[i] = water;
      water2 = getForecast(water);

      this.multi[i] = [
        {
          name: 'Показники з лічильників (вологість)',
          series: this.saveData(dates, values)
        },
        {
          name: 'Прогноз',
          series: this.saveData(dates2, values2)
        }
      ];

      this.multi2[i] = [
        {
          name: 'Показники з лічильників (витрати води)',
          series: this.saveData(dates, water)
        },
        {
          name: 'Прогноз',
          series: this.saveData(dates2, water2)
        }
      ];

      pipeId = this.devices[i].pipe_id;

      let repairDate: any = await this.mySql.getLastRepairDateByDevice(deviceId).toPromise();
      this.pipe_repair_dates[i] = 'Остання дата ремонту труби №' + pipeId + ': ' + moment(repairDate[0].last_repair_date).format('YYYY-MM-DD HH:mm:SS');

      let region: any = await this.mySql.getRegionNameByPipe(pipeId).toPromise();
      region = region[0].name;
      this.messages[i] = 'Увага! Можлива аварія на трубопроводі № ' + pipeId + ' і тоді район ' + region + ' залишиться без водопостачання';

      let humidityLevel: any = await this.mySql.getHumidityLevelByPipe(pipeId).toPromise();
      humidityLevel = humidityLevel[0].humidity_level;

      if ((values2[values2.length - 1] + values2[values2.length - 1] * 0.05) >= humidityLevel) {
        this.showMessages[i] = true;
      } else {
        this.showMessages[i] = false;
      }

    }

  }


  saveData(dates: any[], values: any[]) {
    let data: any[] = [];
    for (let i = 0; i < dates.length; i++) {
      data.push({
        value: values[i],
        name: dates[i]
      });
    }
    return data;
  }

  view: any = [1500, 600];
  legend = true;
  showLabels = true;
  animations = true;
  xAxis = true;
  yAxis = true;
  showYAxisLabel = true;
  showXAxisLabel = true;
  xAxisLabel = 'Дата та час';
  yAxisLabel = 'Вологість';
  timeline = true;
  colorScheme = {
    domain: ['#8A2BE2', '#00BFFF']
  };

  tabChanged(tabIndex: number): void {
    setTimeout(() => {
      if (tabIndex === 1) {
        this.devicePositionsArray.forEach((device, index) => {
          const elem = document.getElementById(`device${index}`) as HTMLElement;
          elem.style.position = 'absolute';
          elem.style.bottom = `${device.y}px`;
          elem.style.left = `${device.x}px`;
        });
      }
    }, 100);
  }

  selectDevice(event: any) {
    this.selectedDevice = event.value;
  }
}


function getForecast(data: any[]) {
  let forecast: any[] = [];

  for (let i = 0; i < data.length - 2; i++) {
    let value = Math.round((data[i] + data[i + 1] + data[i + 2]) / 3);
    forecast.push(value);
  }

  let value = Math.round((data[data.length - 2] + data[data.length - 1] + forecast[forecast.length - 1]) / 3);
  forecast.push(value);

  value = Math.round(forecast[forecast.length - 1] + (1 / 3) * (data[data.length - 1] - data[data.length - 2]));
  forecast.push(value);

  value = Math.round((data[data.length - 2] + data[data.length - 1] + forecast[forecast.length - 1]) / 3);
  value += Math.round((1 / 3) * (forecast[forecast.length - 1] - data[data.length - 1]));
  forecast.push(value);

  return forecast;
}


function getDates(dates: any[]) {
  let newDates: any[] = [];
  for (let i = 1; i < dates.length; i++) {
    newDates.push(dates[i]);
  }
  let lastDate = dates[dates.length - 1];
  let reg = lastDate.substring(14, 11);
  let num = parseInt(lastDate.substring(13, 11)) + 1;

  newDates.push(lastDate.replace(reg, num.toString() + ':'));

  lastDate = newDates[newDates.length - 1];
  reg = lastDate.substring(14, 11);
  num = parseInt(lastDate.substring(13, 11)) + 1;

  newDates.push(lastDate.replace(reg, num.toString() + ':'));

  return newDates;
}
