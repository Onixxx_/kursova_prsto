import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class MySqlService {
  serverPath: string = 'http://localhost:3000';
  currentUser: any;

  constructor(private http: HttpClient) { }

  getAllDevices() {
    return this.http.get(`${this.serverPath}/device`);
  }

  getHistory(deviceId: number) {
    return this.http.get(`${this.serverPath}/historyByDevice/${deviceId}`);
  }

  getRegionNameByPipe(pipeId: number) {
    return this.http.get(`${this.serverPath}/regionByPipe/${pipeId}`);
  }

  getHumidityLevelByPipe(pipeId: number) {
    return this.http.get(`${this.serverPath}/humidity/${pipeId}`);
  }

  getLastRepairDateByDevice(deviceId: number) {
    return this.http.get(`${this.serverPath}/lastRepairDateByDevice/${deviceId}`);
  }

}
