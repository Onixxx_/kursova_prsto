const {response} = require('express');
const express = require('express');
const cors = require('cors');
const {get} = require('http');
const mysql = require('mysql');
const {resolve} = require('path');

const app = express();

//Create
const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'root',
  database: 'water_supply_system',
  insecureAuth : true
})

//Connect
db.connect((err) => {
  if (err) throw err;
  console.log('MySql Connected...');
});

app.listen('3000', () => {
  console.log('Server started on port 3000');
});

app.use(cors());
app.use(express.json());

  app.get('/historyByDevice/:deviceId', (req, res) => {
    let sql = `select * from history_of_readings where device_id = ${req.params.deviceId}`;
    db.query(sql, (err, result) => {
      if (err) throw err;
      res.send(result)
    })
  })

  app.get('/humidity/:pipeId', (req, res) => {
    let sql = `select humidity_level from material join pipe on material.id_material = pipe.material_id where id_pipe = ${req.params.pipeId}`;
    db.query(sql, (err, result) => {
      if (err) throw err;
      res.send(result)
    })
  })
  
  app.get('/device', (req, res) => {
    let sql = 'SELECT * FROM device';
    db.query(sql, (err, result) => {
      if (err) throw err;
      res.send(result)
    })
  })

  app.get('/lastRepairDateByDevice/:deviceId', (req, res) => {
    let sql = `select last_repair_date from device join pipe on device.pipe_id = pipe.id_pipe where id_device = ${req.params.deviceId}`;
    db.query(sql, (err, result) => {
      if (err) throw err;
      res.send(result)
    })
  })


  app.get('/regionByPipe/:pipeId', (req, res) => {
    let sql = `select name from region join pipe on region.id_region = pipe.region_id where id_pipe = ${req.params.pipeId}`;
    db.query(sql, (err, result) => {
      if (err) throw err;
      res.send(result)
    })
  })